﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak6
{
    class Program
    {
        static void Main(string[] args)
        {
            String email = "Someone@gmail.com";
            EmailValidator emailValidator = new EmailValidator();
            if (emailValidator.IsValidAddress(email))
                Console.WriteLine("Your email is valid.");
            else
                Console.WriteLine("Your email is invalid!");

            String password="RPPOONlv4";
            int minLengthOfPassword = 5;
            PasswordValidator passwordValidator = new PasswordValidator(minLengthOfPassword);
            if (passwordValidator.IsValidPassword(password))
                Console.WriteLine("Your password is valid.");
            else
                Console.WriteLine("Your password is invalid!");           
          
        }
    }
}
