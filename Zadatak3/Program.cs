﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak3
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IRentable> rent = new List<IRentable>();
            rent.Add(new Video("Fast and furious"));
            rent.Add(new Book("Hobbit"));

            RentingConsolePrinter rentingConsolePrinter = new RentingConsolePrinter();
            rentingConsolePrinter.DisplayItems(rent);
            rentingConsolePrinter.PrintTotalPrice(rent);
        }
    }
}
