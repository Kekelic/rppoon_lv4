﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak7
{
    class Program
    {
        static void Main(string[] args)
        {
            RegistrationValidator registrationValidator = new RegistrationValidator();
            UserEntry userEntry=UserEntry.ReadUserFromConsole();

            while (!registrationValidator.isUserEntryValid(userEntry))
            {
                Console.WriteLine("Registration is invalid!");
                userEntry = UserEntry.ReadUserFromConsole();
            }
            Console.WriteLine("Registration is valid.");
        }
    }
}
