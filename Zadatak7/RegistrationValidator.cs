﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak7
{
    class RegistrationValidator : IRegistrationValidator
    {
        private IEmailValidatorService emailValidator;
        private IPasswordValidatorService passwordValidator;
        public RegistrationValidator()
        {
            this.emailValidator = new EmailValidator();
            int minLengthOfPassword = 5;
            this.passwordValidator = new PasswordValidator(minLengthOfPassword);
        }

        public bool isUserEntryValid(UserEntry entry)
        {
            return emailValidator.IsValidAddress(entry.Email) && 
                passwordValidator.IsValidPassword(entry.Password);
            
        }
    }
}
