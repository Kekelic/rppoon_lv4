﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak7
{
    interface IRegistrationValidator
    {
        bool isUserEntryValid(UserEntry entry);
    }
}
