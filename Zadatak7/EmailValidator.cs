﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak7
{
    class EmailValidator : IEmailValidatorService
    {
        public EmailValidator() { }

        public bool IsValidAddress(string candidate)
        {
            if (String.IsNullOrEmpty(candidate))
            {
                return false;
            }
            return ContainsAtSign(candidate) && EndsWithSuitableString(candidate);
        }

        private bool ContainsAtSign(string candidate)
        {
            return candidate.Contains("@");
        }

        private bool EndsWithSuitableString(string candidate)
        {
            return candidate.EndsWith(".com") || candidate.EndsWith(".hr");
        }

    }
}
