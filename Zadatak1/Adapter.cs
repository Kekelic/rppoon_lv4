﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak1_i_2
{
    class Adapter : IAnalytics
    {
        private Analyzer3rdParty analyticsService;

        public Adapter(Analyzer3rdParty service)
        {
            this.analyticsService = service;
        }

        private double [][] ConvertData(Dataset dataset)
        {
            IList<List<double>> data = dataset.GetData();
            int rowCount = data.Count;
            int columnCount = data[0].Count;
            double[][] newData = new double[rowCount][];
            for(int i = 0; i < rowCount; i++)
            {
                newData[i] = new double[columnCount];
            }

            int rowCounter = 0;
            int columnCounter = 0;
            foreach(List<double> line in data)
            {
                foreach(double number in line)
                {
                    newData[rowCounter][columnCounter] = number;
                    columnCounter++;
                }
                rowCounter++;
                columnCounter = 0;
            }
            return newData;
        }

        public double[] CalculateAveragePerColumn(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerColumnAverage(data);

        }

        public double[] CalculateAveragePerRow(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerRowAverage(data);
        }
    }
}
