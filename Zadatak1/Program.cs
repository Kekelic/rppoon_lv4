﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak1_i_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset dataset = new Dataset("lv4_zad2.csv");
            IList<List<double>> data = dataset.GetData();
            Analyzer3rdParty analyzer3RdParty = new Analyzer3rdParty();
            Adapter adapter = new Adapter(analyzer3RdParty);

            double[] averagePerRows = adapter.CalculateAveragePerRow(dataset);
            int rowCount = data.Count;
            Console.Write("Average per row: \n");
            for(int i = 0; i < rowCount; i++)
            {
                Console.Write(averagePerRows[i]+" ");
            }

            double[] averagePerColumn = adapter.CalculateAveragePerColumn(dataset);
            int columnCount = data[0].Count;
            Console.WriteLine("\nAverage per column: ");
            for(int i = 0; i < columnCount; i++)
            {
                Console.Write(averagePerColumn[i]+" ");
            }
            Console.WriteLine();
        }
    }
}
