﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak5
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IRentable> rentItems = new List<IRentable>();
            rentItems.Add(new Video("Fast and furious"));
            rentItems.Add(new Book("Hobbit"));
            rentItems.Add(new HotItem(new Video("Black Panther")));
            rentItems.Add(new HotItem(new Book("Harry Potter")));

            List<IRentable> flashSale = new List<IRentable>();
            double discount = 25;
            foreach(IRentable item in rentItems)
            {
                DiscountedItem discountedItem = new DiscountedItem(item, discount);
                flashSale.Add(discountedItem);
            }

            RentingConsolePrinter rentingConsolePrinter = new RentingConsolePrinter();
            rentingConsolePrinter.DisplayItems(flashSale);
            rentingConsolePrinter.PrintTotalPrice(flashSale);
        }
    }
}
