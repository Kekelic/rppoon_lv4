﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak5
{
    class DiscountedItem : RentableDecorater
    {
        private readonly double discount = 5.0;

        public DiscountedItem(IRentable rentable, double percentage) : base(rentable)
        {
            this.discount = percentage;
        }

        public override double CalculatePrice()
        {
            return base.CalculatePrice() - base.CalculatePrice()*discount/100;
        }

        public override String Description
        {
            get
            {
                return base.Description + "now at["+this.discount+ "] % off!";
            }
        } 

    }
}
