﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak5
{
    abstract class RentableDecorater : IRentable
    {
        private IRentable rentable;

        public RentableDecorater(IRentable rentable)
        {
            this.rentable = rentable;
        }

        public virtual double CalculatePrice()
        {
            return rentable.CalculatePrice();
        }

        public virtual String Description
        {
            get
            {
                return rentable.Description;
            }
        }
    }
}
