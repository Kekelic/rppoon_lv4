﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak4
{
    class Program
    {
        static void Main(string[] args)
        {
            //Odgovor na pitanje:
            //U odnosu na prošli zadatak ovjde imamo nadopunjavanje klasa Video i Book pomoću dekoratera
            List<IRentable> rent = new List<IRentable>();
            rent.Add(new Video("Fast and furious"));
            rent.Add(new Book("Hobbit"));
            rent.Add(new HotItem(new Video("Black Panther")));
            rent.Add(new HotItem(new Book("Harry Potter")));

            RentingConsolePrinter rentingConsolePrinter = new RentingConsolePrinter();
            rentingConsolePrinter.DisplayItems(rent);
            rentingConsolePrinter.PrintTotalPrice(rent);
        }
    }
}
